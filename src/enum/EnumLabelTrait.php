<?php

namespace Forwzb\Yii2Api\enum;

/**
 * 枚举标签trait
 */
trait EnumLabelTrait
{
    /**
     * @return string[]
     */
    public static function labels(): array
    {
        return [];
    }

    public static function getLabels(?self ...$enums): array
    {
        $labels = self::labels();
        if ($enums) {
            $_labels = [];
            foreach ($enums as $enum) {
                $_labels[$enum->value] = $labels[$enum->value];
            }
            return $_labels;
        }
        return $labels;
    }

    public function getLabel(): string
    {
        return self::labels()[$this->value];
    }
}