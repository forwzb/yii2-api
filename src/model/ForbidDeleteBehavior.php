<?php

namespace Forwzb\Yii2Api\model;

use yii\base\Behavior;

/**
 * 禁止删除行为
 *
 * 主要在 beforeDelete、delete 跑出异常
 */
class ForbidDeleteBehavior extends Behavior
{
    public function events()
    {
        return ['beforeDelete' => [$this, 'beforeDelete']];
    }

    public function beforeDelete()
    {
        throw new \Yii\base\NotSupportedException();
    }

    public function delete()
    {
        throw new \Yii\base\NotSupportedException();
    }
}